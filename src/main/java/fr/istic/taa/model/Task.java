package fr.istic.taa.model;

import javax.persistence.*;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

/**
 * Created by stephane on 02/10/15.
 */
@EnableAutoConfiguration
@Entity
public class Task implements java.io.Serializable {

	
	 
	
	
    private int id;

    private Developer developer;
    
    
    private Sprint sprint;

    private String name;
    private Date startDate;
    private Date deadline;
    private String comment;
     
    private String etat;
    
    

    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}

    public Task() {

    }

    public Task(String name, Date startDate, Date deadline, String comment ) {
        this.name = name;
        this.startDate = startDate;
        this.deadline = deadline;
        this.comment = comment;
         
    }

    

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne
    @JsonBackReference("dev")
    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    @ManyToOne 
    public Sprint getSprint() {
        return sprint;
    }
    
    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }


    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column
    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    @Column
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    @Column
    public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	@Column
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}