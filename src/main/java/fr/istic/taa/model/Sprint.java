package fr.istic.taa.model;

import java.io.Serializable;
import java.lang.String;
import java.time.LocalDateTime;
import java.util.*;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * Entity implementation class for Entity: Task
 *
 */
@EnableAutoConfiguration
@Entity
public class Sprint implements Serializable {

    private int id;

    private String name;
    private String comment;

    private Date startDate;
    private Date endDate;
    @JsonBackReference("grp")
    private Team team;
    
    private Project project;

    
    private Set<Task> tasks;
    
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}
    public Sprint() {

    }

    public Sprint(Team team, String comment, Date startDate, Date endDate) {
        this.team = team;
        this.comment = comment;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    

    public void setId(int id) {
        this.id = id;
    }

    @OneToOne
    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @OneToMany(mappedBy = "sprint")
    @JsonBackReference("tasks")
    public Set<Task> getTasks() {
        return tasks;
    }
    @JsonBackReference("tache")
    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
    
    @ManyToOne
    @JsonBackReference("sprint")
	public Project getProject() {
		return project;
	}
    @JsonBackReference("sprint")
	public void setProject(Project project) {
		this.project = project;
	}

    @Column
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Column
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   
    
    
}
