package fr.istic.taa.model;


import javax.persistence.*;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import java.util.Set;

@EnableAutoConfiguration
@Entity
public class Team implements Serializable{

    private int id;

    private String name;
    
   
    private Set<Developer> users;
    
    
    private Sprint sprint;
    
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}
    public Team() {
    }

    public Team(String name) {
        this.name = name;
    }

    

    public void setId(int id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "team")
    @JsonBackReference("team")
    public Set<Developer> getUsers() {
        return users;
    }

    public void setUsers(Set<Developer> users) {
        this.users = users;
    }

    @OneToOne(mappedBy = "team")
    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

