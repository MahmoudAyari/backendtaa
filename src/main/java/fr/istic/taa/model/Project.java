package fr.istic.taa.model;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * Entity implementation class for Entity: Task
 *
 */
@EnableAutoConfiguration
@Entity
@Table(name = "PROJECT")
public class Project implements Serializable {

	private int id;

	private String name;

	private Date startDate;

	private Date endDate;

	private String description;

	
	@JsonBackReference("project")
	private Set<Sprint> sprint;

	

	public Project() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Project(String name, Date startDate, Date endDate, String description) {
		super();
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.description = description;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy = "project")
	public Set<Sprint> getSprint() {
		return sprint;
	}
	
	public void setSprint(Set<Sprint> sprint) {
		this.sprint = sprint;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
