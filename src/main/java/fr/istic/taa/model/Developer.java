package fr.istic.taa.model;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.Set;
/**
 * Entity implementation class for Entity: Developer
 *
 */
@EnableAutoConfiguration
@Entity
public class Developer implements Serializable{

    private int id;
    private String name;
    private String lastname;
    private String firstname;
    
    private Set<Task> tasks;
    private Team team;
   
    public Developer() {

    }

    public Developer(String lastname, String firstname) {
        setName(firstname, lastname);
        this.lastname = lastname;
        this.firstname = firstname;
    }

    @Id
   	@GeneratedValue(strategy = GenerationType.AUTO)
   	public int getId() {
   		return id;
   	} 
       

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne
    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Transient
    public String getName() {
        return name;
    }

    public void setName(String firstname, String lastname) {
        this.name = firstname + " " + lastname;
    }

    @Column
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Column
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
    @OneToMany(mappedBy = "developer")
	public Set<Task> getTasks() {
		return tasks;
	}
	
	
	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}
	public void setName(String name) {
		this.name = name;
	}

  
}