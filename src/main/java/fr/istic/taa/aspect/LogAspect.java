package fr.istic.taa.aspect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;



import org.aspectj.lang.annotation.Before;

@Aspect
@Component
public class LogAspect {

    /** Handle to the log file */
    private final Log log = LogFactory.getLog(getClass());

    public LogAspect () {}

    @AfterReturning("execution(* fr.istic.taa.controller.*.*(..))")
    public void logMethodAccessAfter(JoinPoint joinPoint) {
        log.info("***** Completed: " + joinPoint.getSignature().getName() + " *****");
    }


}