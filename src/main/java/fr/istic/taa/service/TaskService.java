package fr.istic.taa.service;

import java.util.List;

import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.istic.taa.model.Sprint;
import fr.istic.taa.model.Task;
import fr.istic.taa.model.Task;
import fr.istic.taa.repository.TaskRepository;

@Service
@Transactional
public class TaskService {

	@Autowired
	 TaskRepository taskRepository; 
	
	
	public List<Task>  findAll( ) {
		return taskRepository.findAll();
	}
	
	public Task findOne(int id) {
		return taskRepository.findOne(id);
	}
	
	public void save(Task task) {
		taskRepository.save(task);
	 }
	
	public void addTask(Task Task) {
		taskRepository.save(Task);
	}
	
	public List<Task> findTasks(int idSprint) {
        return taskRepository.findTasks(idSprint);
	}
	 
	public void deleteTask( int id) {
		taskRepository.delete(id);
	}
	
	 public void deleteAllTasks() {
		taskRepository.deleteAllInBatch();
	}
	 
	public  void updateTask(Task Task){
		taskRepository.saveAndFlush(Task); 
	}
}
