package fr.istic.taa.service;

import java.util.List;

import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.istic.taa.model.Team;
import fr.istic.taa.model.Team;
import fr.istic.taa.repository.TeamRepository;

@Service
@Transactional
public class TeamService {

	@Autowired
	 TeamRepository teamRepository; 
	
	
	public List<Team>  findAll( ) {
		return teamRepository.findAll();
	}
	
	public Team findOne(int id) {
		return teamRepository.findOne(id);
	}
	
	 
	public void addTeam(Team Team) {
		teamRepository.save(Team);
	}
	 
	public void deleteTeam( int id) {
		teamRepository.delete(id);
	}
	
	 public void deleteAllTeam() {
		teamRepository.deleteAllInBatch();
	}
	 
	public  void updateTeam(Team Team){
		teamRepository.saveAndFlush(Team); 
	}
}
