package fr.istic.taa.service;

import java.util.List;

import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.istic.taa.model.Project;
import fr.istic.taa.repository.ProjectRepository;

@Service
@Transactional
public class ProjectService {

	@Autowired
	 ProjectRepository projectRepository; 
	
	
	public List<Project>  findAll( ) {
		return projectRepository.findAll();
	}
	
	public Project findOne(int id) {
		return projectRepository.findOne(id);
	}
	
	public void save(Project project) {
		projectRepository.save(project);
	 }
	
	 
	public void addProject(Project project) {
		projectRepository.save(project);
	}
	 
	public void deleteProject( int id) {
		projectRepository.delete(id);
	}
	
	 public void deleteAllProject() {
		projectRepository.deleteAllInBatch();
	}
	 
	public  void updateProject(Project project){
		projectRepository.saveAndFlush(project); 
	}
}
