package fr.istic.taa.service;

import java.util.List;

import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.istic.taa.model.Project;
import fr.istic.taa.model.Sprint;
import fr.istic.taa.repository.SprintRepository;

@Service
@Transactional
public class SprintService {

	@Autowired
	 SprintRepository sprintRepository; 
	
	
	public List<Sprint>  findAll( ) {
		return sprintRepository.findAll();
	}
	
	public Sprint findOne(int id) {
		return sprintRepository.findOne(id);
	}
	
	public void addSprint(Sprint sprint) {
		sprintRepository.save(sprint);
	 }
	
    
	public List<Sprint> findSprints(int idProject) {
        return sprintRepository.findSprints(idProject);
	}
	
	public void deleteSprint( int id) {
		sprintRepository.delete(id);
	}
	
	 public void deleteAllSprints() {
		sprintRepository.deleteAllInBatch();
	}
	 
	public  void updateSprint(Sprint sprint){
		sprintRepository.saveAndFlush(sprint); 
	}
}