package fr.istic.taa.service;

import java.util.List;

import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.istic.taa.model.Developer;
import fr.istic.taa.model.Developer;
import fr.istic.taa.repository.DeveloperRepository;

@Service
@Transactional
public class DeveloperService {

	@Autowired
	 DeveloperRepository developerRepository; 
	
	
	public List<Developer>  findAll( ) {
		return developerRepository.findAll();
	}
	
	public Developer findOne(int id) {
		return developerRepository.findOne(id);
	}
	
	public void addDeveloper(Developer Developer) {
		developerRepository.save(Developer);
	}
	 
	public void deleteDeveloper( int id) {
		developerRepository.delete(id);
	}
	
	 public void deleteAllDeveloper() {
		developerRepository.deleteAllInBatch();
	}
	 
	public  void updateDeveloper(Developer Developer){
		developerRepository.saveAndFlush(Developer); 
	}
}
