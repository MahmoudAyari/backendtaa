package fr.istic.taa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.istic.taa.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer>{

	Project findByName(String name);


}