package fr.istic.taa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

 
import fr.istic.taa.model.Task;;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer>{

	Task findByName(String name);

	@Query( "SELECT t FROM Task t WHERE t.sprint.id=:t_id")
    public List<Task> findTasks(@Param("t_id") int id);
}