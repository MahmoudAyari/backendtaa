package fr.istic.taa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.istic.taa.model.Developer;

@Repository
public interface DeveloperRepository extends JpaRepository<Developer, Integer>{

	


}