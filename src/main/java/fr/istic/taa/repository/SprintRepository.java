package fr.istic.taa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.istic.taa.model.Sprint;

@Repository
public interface SprintRepository extends JpaRepository<Sprint, Integer>{

	Sprint findByName(String name);

	@Query( "SELECT s FROM Sprint s WHERE s.project.id=:p_id")
    public List<Sprint> findSprints(@Param("p_id") int id);
}