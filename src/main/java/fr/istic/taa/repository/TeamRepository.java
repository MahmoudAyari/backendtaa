package fr.istic.taa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.istic.taa.model.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer>{

	Team findByName(String name);


}