

package fr.istic.taa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;




@SpringBootApplication
@Configuration
@ComponentScan(basePackages ={"fr.istic.taa.aspect",
   "fr.istic.taa.controller",
   "fr.istic.taa.model",
   "fr.istic.taa.repository",
   "fr.istic.taa.service",
   "fr.istic.taa.config"})
public class MainApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MainApplication.class, args);
	}

}
