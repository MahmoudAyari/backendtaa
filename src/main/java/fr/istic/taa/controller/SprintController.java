package fr.istic.taa.controller;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
 
import fr.istic.taa.model.Project;
import fr.istic.taa.model.Sprint;
import fr.istic.taa.service.ProjectService;
import fr.istic.taa.service.SprintService;




@RestController
@RequestMapping(value = "/sprints", produces = MediaType.APPLICATION_JSON_VALUE)
public class SprintController {

	@Autowired
	ProjectService projectService ;
	@Autowired
	SprintService sprintService ;
	
	@RequestMapping(  value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Sprint getSprint(@PathVariable("id") int id) {
		Sprint sprint=sprintService.findOne(id);
		return sprint;
	}
	
	
	@RequestMapping(value = "/add/{idProject}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Sprint> addSprint(@RequestBody Sprint sprint, @PathVariable("idProject") int idProject) {

		Project project = projectService.findOne(idProject);
		sprint.setProject(project);
		sprintService.addSprint(sprint);
		return new ResponseEntity<Sprint>(sprint, HttpStatus.OK);

	}
	
	
	@RequestMapping(value = "/project/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Sprint> findAllSprints(@PathVariable("id") int id) {
		return sprintService.findSprints(id);

	}

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Sprint> findAll() {
		return sprintService.findAll();
	}
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public void deleteAllSprints() {
		sprintService.deleteAllSprints();
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Sprint> updateSprint(@RequestBody Sprint sprint) {
		sprintService.updateSprint(sprint);
		return new ResponseEntity<Sprint>(sprint, HttpStatus.OK);
	}

	
}
