package fr.istic.taa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.istic.taa.model.Team;
import fr.istic.taa.service.TeamService;


@RestController
@RequestMapping(value = "/teams", produces = MediaType.APPLICATION_JSON_VALUE)
public class TeamController {

	@Autowired
	TeamService teamService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Team getTeam(@PathVariable("id") int id) {
		Team Team = teamService.findOne(id);
		return Team;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Team> addTeam(@RequestBody Team object) {

		Team Team = new Team();
		Team.setName(object.getName());
         teamService.addTeam(Team);
		return new ResponseEntity<Team>(object, HttpStatus.OK);

	}

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Team> findAll() {
		return teamService.findAll();

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteTeam(@PathVariable int id) {
		teamService.deleteTeam(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public void deleteAllTeam() {
		teamService.deleteAllTeam();
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<Team> updateTeam(Team Team) {
		teamService.updateTeam(Team);
		return new ResponseEntity<Team>(Team, HttpStatus.OK);
	}

}
