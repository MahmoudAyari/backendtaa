package fr.istic.taa.controller;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.istic.taa.model.Project;
import fr.istic.taa.model.Sprint;
import fr.istic.taa.model.Task;
import fr.istic.taa.service.SprintService;
import fr.istic.taa.service.TaskService;



@RestController
@RequestMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskController {

	@Autowired
	TaskService taskService;
	
	@Autowired
	SprintService sprintService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Task getTask(@PathVariable("id") int id) {
		Task Task = taskService.findOne(id);
		return Task;
	}

	 

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Task> findAll() {
		return taskService.findAll();

	}
	
	@RequestMapping(value = "/add/{idSprint}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Task> addTask(@RequestBody Task task, @PathVariable("idSprint") int idSprint) {

		Sprint sprint = sprintService.findOne(idSprint);
		task.setSprint(sprint);
		taskService.addTask(task);
		return new ResponseEntity<Task>(task, HttpStatus.OK);

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteTask(@PathVariable int id) {
		taskService.deleteTask(id);
	}

	
	@RequestMapping(value = "/sprint/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Task> findAllTasks(@PathVariable("id") int id) {
		return taskService.findTasks(id);

	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public void deleteAllTask() {
		taskService.deleteAllTasks();
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Task> updateTask(@RequestBody Task task) {
		taskService.updateTask(task);
		return new ResponseEntity<Task>(task, HttpStatus.OK);
	}
}
