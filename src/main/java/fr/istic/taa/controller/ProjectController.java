package fr.istic.taa.controller;
 
import java.util.List; 
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.RestController;

import fr.istic.taa.model.Project; 
import fr.istic.taa.service.ProjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;


@RestController
@RequestMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectController {

	@Autowired
	ProjectService projectService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Project getProject(@PathVariable("id") int id) {
		Project project = projectService.findOne(id);
		return project;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Project> addProject(@RequestBody Project project) {
        projectService.save(project);
        return new ResponseEntity<Project>(project, HttpStatus.OK);

	}

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Project> findAll() {
		return projectService.findAll();

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteProject(@PathVariable int id) {
		projectService.deleteProject(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public void deleteAllProject() {
		projectService.deleteAllProject();
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<Project> updateProject(Project project) {
		projectService.updateProject(project);
		return new ResponseEntity<Project>(project, HttpStatus.OK);
	}

}
