package fr.istic.taa.controller;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
 
import fr.istic.taa.model.Developer; 
import fr.istic.taa.service.DeveloperService; 


@RestController
@RequestMapping(value = "/developers", produces = MediaType.APPLICATION_JSON_VALUE)
public class DeveloperController {

	
	@Autowired
	DeveloperService developerService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Developer getDeveloper(@PathVariable("id") int id) {
		Developer developer = developerService.findOne(id);
		return developer;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Developer> addDeveloper(@RequestBody Developer object) {

		Developer developer = new Developer();
		developer.setName(object.getName());
		 

		developerService.addDeveloper(developer);

		return new ResponseEntity<Developer>(object, HttpStatus.OK);

	}

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Developer> findAll() {
		return developerService.findAll();

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteDeveloper(@PathVariable int id) {
		developerService.deleteDeveloper(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public void deleteAllDeveloper() {
		developerService.deleteAllDeveloper();
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<Developer> updatedeveloper(Developer developer) {
		developerService.updateDeveloper(developer);
		return new ResponseEntity<Developer>(developer, HttpStatus.OK);
	}
	

	
}
