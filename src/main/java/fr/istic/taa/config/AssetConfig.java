package fr.istic.taa.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import fr.istic.taa.aspect.LogAspect;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = {"fr.istic.taa.aspect"})

public class AssetConfig {

    @Bean   
    public LogAspect myLogger(){
       return new LogAspect();
    }
}
