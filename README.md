# README #

#Contributeurs AYARI Mahmoud & SOUDOU Zakariae 

Back-End en Spring-boot, Jpa, maven ,spring-aop

Lancer le serveur de base de données : ./run-hsqldb-server.sh

Lancer le serveur undertow :  mvn spring-boot:run


urls pour accéder aux flux Json

http://localhost/projects/all : liste de tous les projets

http://localhost/sprints/all :  liste de tous les sprints

http://localhost/tasks/all :  liste de tous les tâches

http://localhost/sprints/project/1 : liste de tous les sprints du projet qui a l'id 1

http://localhost/tasks/sprint/1 : liste de tous les tâches du sprint  qui a l'id 1